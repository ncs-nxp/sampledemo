package sg.ncs.product.sampledemo.common.exception;


import sg.ncs.product.sampledemo.common.constant.MsgCode;

/**
* Business exception
*
*/
public class InfoException extends RuntimeException{
    private MsgCode msgCode;

    public InfoException(String message) {
        super(message);
    }

    public InfoException(MsgCode msgCode) {
        super(msgCode.getMessage());
        this.msgCode = msgCode;
    }

    public MsgCode getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(MsgCode msgCode) {
        this.msgCode = msgCode;
    }
}
